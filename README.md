# Google Analytics Checkout Event Tracker for Magento 2
The module provides vendors functionality to track checkout events in google analytics.
## Manually Installation

Magento2 module installation is very easy, please follow the steps for installation-

=> Download and unzip the respective extension zip and create GrossmanInteractive(vendor) and GoogleAnalyticsCheckoutTracker(module) name folder inside your magento/app/code/ directory and then move all module's files into magento root directory Magento2/app/code/GrossmanInteractive/GoogleAnalyticsCheckoutTracker/ folder.

## Run following command via terminal from magento root directory 
  
     $ php bin/magento setup:upgrade
     $ php bin/magento setup:di:compile
     $ php bin/magento setup:static-content:deploy

=> Flush the cache and reindex all.

now module is properly installed

## Code explanations 

https://webkul.com/blog/create-hello-module-in-magento2
